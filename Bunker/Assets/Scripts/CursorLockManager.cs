﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorLockManager : MonoBehaviour {

	//by Austin s1604097

	// Use this for initialization
	void Start () {
		Cursor.lockState = CursorLockMode.Locked;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Lockcursor(){
		Cursor.lockState = CursorLockMode.Locked;
	}

	public void UnlockCursor(){
		Cursor.lockState = CursorLockMode.None;
	}
}
