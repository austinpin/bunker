﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Strobe : MonoBehaviour
{
    private List<Material> materialInstances;
    [SerializeField]
    private List<int> materialsToChange;

    // Used for determining ping pong offset:
    private float pongOffset = 0;

    // The color's we'll be setting our object to:
	public Color startColor;
	public Color endColor;

	public float changeSpeed = 0.4f;
    // Changing this will decide whether to randomise the strobe speed or not:
    public bool randomiseSpeed = false;

	// Use this for initialization
	void Start()
    {
        // Initalise our material instances list:
        materialInstances = new List<Material>();

        // Get our renderer:
        Renderer renderer = GetComponent<Renderer>();

        // Populate our list of instantiated materials:
        foreach(int i in materialsToChange)
        {
            // Check to make sure i is a valid material index:
            if (i < renderer.materials.Length)
            {
                materialInstances.Add(renderer.materials[i]);
                renderer.materials[i] = materialInstances[i];
            }
            // Log a warning if it's not:
            else Debug.LogWarning(name + " attempted to access material " + i + " on an object with only " + renderer.materials.Length + " materials!");
        }
        pongOffset = Random.Range(0.0f, 1.0f);

        //Debug.Log(name + "'s pongOffset: " + pongOffset);

        // Randomise speed if randomiseSpeed is set:
        if (randomiseSpeed) changeSpeed += pongOffset;
	}
	
	// Update is called once per frame
	void Update()
    {
        float frag = Mathf.PingPong((Time.time + pongOffset) * changeSpeed, 1);

        Color newColor = Color.Lerp (startColor, endColor, frag);

        foreach(Material mat in materialInstances)
        {
            mat.SetColor("_EmissionColor", newColor);
        }
	}

	void OnDestroy()
    {
        // Delete our instanced material:
        materialInstances = null; //This does not destroy the material by the way
    }
}
