﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogDevice : MonoBehaviour, IInteractable {

	//Log system by Austin s1604097

	public string title;
	[TextArea]
	public string message;

	LogManager LM;

	void Start(){
		//find the log manager
		LM = GameObject.Find ("GameManager").GetComponent<LogManager> ();
	}

	//if intereacted with, create a new log from the log manager
	public void Interacted(){
		LM.NewLogs (title,message);
		Destroy (gameObject);
	}
}
