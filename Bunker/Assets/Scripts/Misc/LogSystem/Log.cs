﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Log  {

	//Log system by Austin s1604097
	//individual values of a log

	public string title;
	public string message;

	public Log(string _title, string _message){
		title = _title;
		message = _message;
	}
}
