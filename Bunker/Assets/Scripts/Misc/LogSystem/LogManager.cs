﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogManager : MonoBehaviour
{

	//Log system by Austin s1604097

	//note the system has a default button in place with ID -1 for other purposes

	public List<Log> foundLogs=new List<Log>();

	//unlocking and locking the cursor to allow the buttons to be pressed.
	CursorLockManager CLM;

	//button to instance, where to instance it, hiding the log overlay, text box which will display the text
	public GameObject newButton;
	Transform content; //content is where you spawn items inside a scroll view.
	public GameObject noteParent;
	Text messageText;

	//assigning a unique value to the buttons and where to spawn them
	int totalButtons=1;
	float yPos;

	// Use this for initialization
	void Start () {
		//grab values
		//disable the note overlay on startup,

		CLM = GameObject.Find ("GameManager").GetComponent<CursorLockManager>();
		content = noteParent.transform.GetChild (0).GetChild (0).GetChild(0);
		noteParent.SetActive (false);
		messageText = noteParent.transform.GetChild(1).GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		ShowNotes ();
	}

	void ShowNotes(){
		//on key press, toggle to active state of the logs , and lock or unlock the mouse
		if(Input.GetButtonDown("Show Logs")){
			if (noteParent.activeInHierarchy) {
				noteParent.SetActive (false);
				CLM.Lockcursor ();

			} else {
				noteParent.SetActive (true);
				CLM.UnlockCursor ();
			}
		}
	}

	public void NewLogs(string _title, string _message){
		//list of found logs
		foundLogs.Add (new Log (_title, _message));

		//create new button
		GameObject button = Instantiate (newButton, content) as GameObject;

		//grab its ID
		ButtonIdentifier BI = button.GetComponent<ButtonIdentifier> ();
		BI.ID = totalButtons - 1;
		//add a new click event to the button to perform a function here with its ID
		button.GetComponent<Button>().onClick.AddListener(delegate {SelectLog(BI.ID);});

		//grab its transform and spawn it at intervals of -40 (size of button is -30, gap is -10, first gap is only -10)
		RectTransform buttonRect = button.GetComponent<RectTransform> ();
		//-10, -50, -90, (series)
		yPos=(totalButtons*-40)-10;
		//set its position based on its parent
		buttonRect.anchoredPosition = new Vector2 (0,yPos);
		totalButtons++;

		//set button title
		Text titleText = button.GetComponentInChildren<Text> ();
		titleText.text = _title;

		//messageText.text = _message;
	}

	public void SelectLog(int newSelect){
		//if its the default button
		if (newSelect == -1) {
			Debug.Log ("message");
		} else {
			//show message from log list based on given int
			messageText.text = foundLogs [newSelect].message;
		}


	}
}
