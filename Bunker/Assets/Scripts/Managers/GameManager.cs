﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private string dataPath = "test.json";
    [SerializeField] private string savePath = "save.json";

	public static GameManager Instance;

	[SerializeField]
    public GameData GameData;
    
	public PlayerEntity Player;

    public string LoadJson(string path)
    {
        string fullPathName = Application.dataPath + "/" + path;

        Debug.Log("Loading from file " + fullPathName + "...");
        string json = File.ReadAllText(fullPathName);

        return json;
    }

    void Awake()
    {
        if (Instance != null)
        {
			Debug.LogWarning(name + ": GameManager already in scene! Exiting...");
	        Destroy(this);
        }
		else
        {
            Instance = this;
            DontDestroyOnLoad(Instance);
        }

    	Player = GameObject.FindWithTag("Player").GetComponent<PlayerEntity>();    

        string fullPathName = Application.dataPath + "/" + dataPath;
        string json         = File.ReadAllText(fullPathName);

        GameData = JsonUtility.FromJson<GameData>(json);

        Load();
    }

    public void UpdateCheckpoint(Transform inCheckpointPosition)
    {
        Save();
    }

	private void Update()
	{
		if(Input.GetKeyDown("r"))
		{
			Load();
		}
	}

    public void Save()
    {
        string json = JsonUtility.ToJson(GameData, true);
        string fullPathName = Application.dataPath + "/" + savePath;

        Debug.Log("Saved under: " + fullPathName);
        File.WriteAllText(fullPathName, json);
    }

    public Objective GetObjective(string givenName)
    {
        return GameData.ObjectivesMaster.FirstOrDefault(O => O.Name == givenName);
    }

	public void Load()
	{
        string fullPathName = Application.dataPath + "/" + savePath;
        
		Debug.Log("Loading from file " + fullPathName + "...");
		string json = File.ReadAllText(fullPathName);
		
		GameData = JsonUtility.FromJson<GameData>(json);

        Player.Data = GameData.Player;
	}
}


