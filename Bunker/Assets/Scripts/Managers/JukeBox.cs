﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JukeBox : MonoBehaviour, ITriggerable
{
	[SerializeField] private AudioSource audioSource;
	[SerializeField] private AudioClip[] tracks;
	[SerializeField] private int  initialTrack;
	[SerializeField] private bool playOnAwake;

	private int currentTrack;

	void Start()
	{
		if(tracks.Length == 0)
		{
			Debug.Log(name + ": \"Add tracks! Stupid!\"");

			gameObject.SetActive(false);
		}

		currentTrack = initialTrack;

		if(initialTrack >= tracks.Length) currentTrack = tracks.Length;

		if(currentTrack > -1) audioSource.clip = tracks[currentTrack];

		if(playOnAwake)
		{
			audioSource.Play();
		}
		else
		{
			audioSource.Stop();
		}
	}

	public void ChangeTrack( int trackNo)
	{
		if(trackNo >= tracks.Length)
		{
			audioSource.clip = tracks[trackNo];

			audioSource.Play();
		}
	}

	public void Trigger()
	{
		++currentTrack;
		if(currentTrack >= tracks.Length) currentTrack = 0;

		audioSource.clip = tracks[currentTrack];
		audioSource.Play();
	}
}
