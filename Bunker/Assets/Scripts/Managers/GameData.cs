﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class for serializing game Data:
[System.Serializable]
public class GameData
{
    // Where our master list for all objectives lives. Any objectives found ina given level will originate in this list:
    [SerializeField]
    public List<Objective> ObjectivesMaster;

    [SerializeField]
	public PlayerData Player;
}
