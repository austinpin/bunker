using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIController : MonoBehaviour
{
	public static GUIController Instance;
	void Awake()
	{
		if(Instance != null)
		{
			Debug.LogWarning("You have multiple GUIControllers in your scene! Fixing...");
			Destroy(gameObject);
		}
		else 
		{
			Instance = this;
		}
	}

	void Start()
	{
		infoText.text = "";
		shop.gameObject.SetActive(false);
	}

	void Update()
	{
		InfoTextUpdate();
		ObjectivesUpdate();
	}

	[SerializeField]
	private Text infoText;

	public void InfoText_Print(string s, float time, bool overrideExisting)
	{
		// Only override what's already there if we wanted to:
		if(overrideExisting || infoText.text == "")
		{
			infoText.text = s;
			infoTextUpdateTime = time;
		}
	}

	float infoTextUpdateTime = 0f;
	void InfoTextUpdate()
	{
		if(infoTextUpdateTime != 0f)
		{
			infoTextUpdateTime -= Time.deltaTime;

			if(infoTextUpdateTime < 0f)
			{
				infoTextUpdateTime = 0f;
				infoText.text = "";
			}
		}
	}

	[SerializeField]
	private UIInventory shop;

	public void DisplayShopMenu(Shop in_shop)
	{
		// Populate Shop items:
		if(in_shop != null)
		{
			shop.gameObject.SetActive(true);
			// Stuff goes here:
			shop.Populate(in_shop);
		}
		else
		{
			// Stuff goes here:
			shop.Populate(null);
			shop.gameObject.SetActive(false);			
		}
	}

	[SerializeField] private Text objectiveText;
	[SerializeField] private RectTransform objectiveFrame;

	private float objectiveDisplayTime = 0.0f;

	private void ObjectivesUpdate()
	{
		objectiveDisplayTime -= Time.deltaTime;
		
		if(objectiveDisplayTime < 0) HideObjectives();
	}
	public void DisplayObjectives(List<Objective> objectives, float time)
	{
		Debug.Log("Number of objectives: " + objectives.Count);

		if(objectives.Count > 0)
		{
			objectiveFrame.gameObject.SetActive(true);
			objectiveText.text = "";
			objectiveDisplayTime = time;

			foreach(Objective objective in objectives)
			{
				objectiveText.text += objective.Title + "\n > ";
		
				objectiveText.text += objective.Description + "\n";
			}
		}
	}
	public void HideObjectives()
	{
		objectiveFrame.gameObject.SetActive(false);

		objectiveDisplayTime = 0.0f;
	}

	[SerializeField] private Text HealthText;
	[SerializeField] private Text ArmorText;
	[SerializeField] private Text ShieldText;
	
	public void UpdateStats(float health, float armor, float shield)
	{
		HealthText.text = health.ToString();
		ArmorText.text = armor.ToString();
		ShieldText.text = shield.ToString();
	}
}
