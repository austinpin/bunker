using UnityEngine;

public class Flare : MonoBehaviour
{
	[SerializeField] private Shot shot;

	void OnCollisionEnter(Collision collision)
	{
		Entity entity = collision.collider.GetComponent<Entity>();

		if(entity != null) entity.OnDamage(shot);

		Destroy(gameObject);
	}
}
