using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/* - Rocket.cs
 * By Nathan Penny
 * This Projectile keeps all entities that pass near enough to it to an entity list,
 * and removes them if they leave. When the rocket colliders with something, all entities
 * caught near the explosion are deal damage to, and an explosive particle prefab is instantiated.
 */
public class Rocket : MonoBehaviour
{
    [SerializeField] private new ParticleSystem particleSystem;
	[SerializeField] private Shot shot;

	private List<Entity> toDamage;

	void Awake()
	{
		toDamage = new List<Entity>();
	}

	void OnTriggerEnter(Collider collider)
	{
		Entity entity = collider.GetComponent<Entity>();

		if(entity != null)
		{
			toDamage.Add(entity);
		}
	}

	void OnTriggerExit(Collider collider)
	{
		Entity entity = collider.GetComponent<Entity>();

		if(entity != null)
		{
			toDamage.Remove(entity);
		}
	}

	void OnCollisionEnter(Collision collision)
	{
		foreach(Entity ent in toDamage)
		{
			ent.OnDamage(shot);
		}

        Instantiate(particleSystem, transform.position, Quaternion.identity);
		Destroy(gameObject);
	}
}
