﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject != null && col.gameObject.tag == "Player")
        {
            GameManager.Instance.UpdateCheckpoint(transform);
            Debug.Log("Entered Checkpoint!");
        }
    }
}
