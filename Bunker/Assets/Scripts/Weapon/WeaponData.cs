using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/* - WeaponData.cs
 * - By Nathan Penny
 * This class defines metadata about a given weapon's current state.
 * Used by the Shop to store weapon, and the player to create weapons.
 */
[System.Serializable]
public class WeaponData
{
	public WeaponData(WeaponData data)
	{
		Name             = data.Name;
		Description      = data.Description;
		Value            = data.Value;
        Icon             = data.Icon;
        Path             = data.Path;
	}

    [SerializeField] public string Name;
    [SerializeField] public string Description;
	// Specifies the path in the Resources/ folder to check for our icon.
	[SerializeField] public string Icon;
	// Specifies where the path to our actual weapon is.
	[SerializeField] public string Path;
	[SerializeField] public uint   Value;
}
