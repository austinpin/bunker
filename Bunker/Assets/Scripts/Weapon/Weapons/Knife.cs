﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knife : Weapon
{
	[SerializeField] public Shot shot;
	[SerializeField] public float range;
	[SerializeField] public float chargeTime;

	private float charge = 0.0f;

	void Update()
	{
		if(!Input.GetButton("Fire1"))
		{
			if(charge > chargeTime)
			{
				RaycastHit rayHit;
				if (Physics.Raycast(camera.position, camera.forward, out rayHit, range))
				{
					// We've hit something:
					Entity subject = rayHit.collider.gameObject.GetComponent<Entity>();
			
					if (subject != null)
					{
						// Deal damage to the subject:
						subject.OnDamage(shot);
					}
				}
			}

			charge = 0.0f;
		}
	}

    public override void Fire()
    {
		charge += Time.deltaTime;
    }
}
