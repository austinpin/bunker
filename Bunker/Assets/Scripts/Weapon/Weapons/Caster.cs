﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* - Caster.cs
 * - By Nathan Penny
 * This weapon fires by instantiating a projectile prefab and adding velocity to it.
 *  It is used by the Flaregun, the Rocket Launcher, and the Super Rocket Launcher.
 */
public class Caster : Weapon
{
	[SerializeField] private GameObject projectilePrefab;
	[SerializeField] private float projectileSpeed;
	[SerializeField] private float fireRate;
	[SerializeField] private uint  magMax;
	[SerializeField] private uint  magazine;
	[SerializeField] private uint  reserve;
	[SerializeField] private float reloadSpeed;

	[SerializeField] private AudioSource audioSource;
	[SerializeField] private AudioClip[] fireSounds;

	// reloading keeps track of whether we are reloading or not:
	private bool  reloading = false;
	// reloadFrac keeps time of our reload:
	private float reloadFrac = 0.0f;
	// ActionFrac keeps time or our fire rate:
	private float actionFrac = 0.0f;

	private void Update()
	{
		if(actionFrac > 0) actionFrac -= Time.deltaTime;

		if(reloading)
		{
			reloadFrac += Time.deltaTime;

			if(reloadFrac > reloadSpeed)
			{
				if(reserve > 0)
				{
					++magazine;
					--reserve;

					reloadFrac = 0.0f;
				}
				else
				{
					Debug.Log("No Ammo!");

					// Stop reloading we have nothing left in reserve:
					reloading = false;
				}
			}

			if(magazine == magMax)
			{
				Debug.Log("Full!");
				
				// Stop reloading if our weapon is full:
				reloading = false;
			}
		}
	}

    public override void Fire()
    {
		// Prevent firing if we're out of ammo:
		if(magazine < 1)
		{
			if(reserve < 1)
			{
				Debug.Log("No Ammo!");
			}
			else
			{
				Debug.Log("reloading...");

				reloading = true;
			}

			return;
		}

		// Prevent us from firing faster than fireFrac:
		if(actionFrac > 0) return;

		// Cancel our reloading on firing:
		if(reloading) reloading = false;

		audioSource.clip = fireSounds[Random.Range(0, fireSounds.Length)];
		audioSource.Play();

		--magazine; actionFrac = fireRate;

		// Create a projectile 1 meter in front of us, and get it's rigidbody component:
		Rigidbody projectile = Instantiate(projectilePrefab, barrel.position + barrel.forward, transform.rotation).GetComponent<Rigidbody>();
		if(projectile != null)
		{
			projectile.name = projectile.name;
			projectile.velocity = (camera.forward * projectileSpeed);
		}
		else
		{
			Debug.LogError("Fired projectile doesn't have a Rigidbody!");
		}
	}
}
