﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* - Shotgun.cs
 * - By Nathan Penny
 * This weapon fires a blast of several pellet's at once.
 */
public class Shotgun : Weapon
{
	[SerializeField] public Shot shot;
	[SerializeField] public uint pelletCount;

	[SerializeField] public float range;
	[SerializeField] public float spread;

	[SerializeField] public float fireRate;
	[SerializeField] public uint  magMax;
	[SerializeField] public uint  magazine;
	[SerializeField] public uint  reserve;

	[SerializeField] public float reloadSpeed;
	[SerializeField] private AudioSource audioSource;

	private bool reloading = false;

	// ReloadFrac keeps time of our reload:
	[SerializeField] private float reloadFrac = 0.0f;
	// ActionFrac keeps time or our fire rate:
	[SerializeField] private float actionFrac = 0.0f;

	private void Update()
	{
		if(actionFrac > 0) actionFrac -= Time.deltaTime;

		if(reloading)
		{
			reloadFrac += Time.deltaTime;

			if(reloadFrac > reloadSpeed)
			{
				if(reserve > 0)
				{
					++magazine;
					--reserve;

					reloadFrac = 0.0f;
				}
				else
				{
					Debug.Log("No Ammo!");

					// Stop reloading we have nothing left in reserve:
					reloading = false;
				}
			}

			if(magazine == magMax)
			{
				Debug.Log("Full!");
				
				// Stop reloading if our weapon is full:
				reloading = false;
			}
		}
	}

    public override void Fire()
    {
		// Prevent firing if we're out of ammo:
		if(magazine < 1)
		{
			if(reserve < 1)
			{
				Debug.Log("No Ammo!");
			}
			else
			{
				Debug.Log("Reloading...");

				reloading = true;
			}

			return;
		}

		// Prevent us from firing faster than fireFrac:
		if(actionFrac > 0) return;

		// Cancel our reloading on firing:
		if(reloading) reloading = false;

        Debug.Log("BOOM!");
		audioSource.Play();

		--magazine; actionFrac = fireRate;

        for(int i = 0; i < pelletCount; ++i)
        {
            RaycastHit rayHit;
            if(Physics.Raycast(transform.position, camera.forward, out rayHit, range))
            {
                // We've hit something:
                Entity subject = rayHit.collider.gameObject.GetComponent<Entity>();

                if (subject != null)
                {
                    // Deal damage to the subject:
                    subject.OnDamage(shot);
                }
            }
        }
    }
}
