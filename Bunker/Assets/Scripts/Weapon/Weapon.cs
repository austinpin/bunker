﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Enumerated type used to describe various forms of attack:
[System.Serializable]
public enum ShotType
{
    BULLET    = 0,
	FIRE      = 1,
	EXPLOSIVE = 2,
};

/*
 * Formal bullet type. Used (should be used)for all firearms
 * type events, and potentially any other event's where dam-
 * age is done to entity-derived figures:
 * 
 * - By Nathan Penny
 */
[System.Serializable]
public struct Shot
{
	public ShotType Type;
	public uint Damage;

    public Shot(ShotType ammoType, uint damage)
    {
        Type = ammoType;
        Damage = damage;
    }
};

/* - Weapon.cs
 * - By Nathan Penny
 *
 * Weapon type. Used for casting all manner of attacks from
 * the player, and potentially, other entities:
 */
public abstract class Weapon : MonoBehaviour
{
	[HideInInspector] public new Transform camera;
	[SerializeField] protected Transform barrel;

    public abstract void Fire();
}

