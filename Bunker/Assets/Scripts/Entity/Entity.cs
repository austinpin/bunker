﻿/*
 * - Entity.cs
 * - By Nathan Penny
 *
 * You have no idea how much code this used to be. Minimalising your code
 * base is a good, GOOD thing, people.
 */

public interface Entity
{
    void OnDamage(Shot damage);
}
