using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct DamageMask
{
	public ShotType Type;
	public float DamageModifier;
}

[System.Serializable]
public struct BreakableData
{
	public float health;
};

/* - Breakable.cs
 * - By Nathan Penny
 * This entity is a generic breakable object that
 * will only die if hit with the right attack. What constitutes the right
 * attack is up to you.
 */
public class Breakable : MonoBehaviour, Entity
{
	public BreakableData Data;
    public List<DamageMask> DamageMasks;

    [SerializeField] private new ParticleSystem particleSystem;

	public void OnDamage(Shot shot)
	{
		Debug.Log(name + ": \"Hit for " + shot.Damage + " damage!\"");

        foreach(DamageMask damageMask in DamageMasks)
        {
            if(damageMask.Type == shot.Type)
            {
                Data.health -= shot.Damage * damageMask.DamageModifier;
            }
        }

		if(Data.health < 0)
		{
			Debug.Log(name + " was killed!");

            Instantiate(particleSystem, transform.position, Quaternion.identity);
			Destroy(gameObject);
		}
	}
}
