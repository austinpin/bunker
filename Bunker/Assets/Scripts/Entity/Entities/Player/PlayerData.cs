﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct PlayerData
{
    [System.Serializable]
    public struct Position
    {
        public Position(float in_x, float in_y, float in_z)
        {
            x = in_x;
            y = in_y;
            z = in_z;
        }
        public float x; public float y; public float z;
    };

    [SerializeField]
    public Position RestorePos;
    [SerializeField]
    public Position RestoreRot;
    
    [SerializeField]
    public float Health;
    [SerializeField]
    public float Shield;
    [SerializeField]
    public float Armor;
    [SerializeField]
    public float ArmorEffectiveness;

    [SerializeField] public List<Objective> Objectives;
    [SerializeField] public uint Credits;
    //    [SerializeField] public WeaponData StartingWeapon;
    [SerializeField] public List<WeaponData> Inventory;
	/* The starting weapon is a unique weapon; it can't be sold and therefore needs no description or icon. */
    [SerializeField] public WeaponData StartingWeapon;
}