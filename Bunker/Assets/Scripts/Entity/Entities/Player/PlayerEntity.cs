﻿using System.Collections; 
using System.Collections.Generic; 
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerEntity : MonoBehaviour, Entity
{
	[SerializeField] private AudioSource audioSource;
	[SerializeField] private AudioClip   painSound;
	[SerializeField] private AudioClip   deathSound;

	public PlayerData Data;
	public Shop IndexedShop = null;

    [SerializeField] private bool GetDataFromGameManager;

    private Weapon weapon;

	[SerializeField] private int weaponIndex = -1;

	private void Awake()
	{
		DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        if (GetDataFromGameManager)
        {
            Data = GameManager.Instance.GameData.Player;
        }

        SetActiveWeapon(Data.StartingWeapon);
    }

    private void SetActiveWeapon(WeaponData data)
    {
		if(weapon != null) Destroy(weapon.gameObject);

        Weapon newWeapon = Resources.Load<GameObject>(data.Path).GetComponent<Weapon>();

        if(newWeapon == null)
        {
            Debug.LogError("Weapon in player inventory is missing weapon script!");
        }
		else
		{
			Transform camera = GameObject.FindWithTag("MainCamera").transform;

			weapon = Instantiate(newWeapon, camera);

			weapon.camera = camera;
		}
    }

    public void OnDamage(Shot shot)
    {
    	// Shield damage code:
        if (Data.Shield > 0)
        {
            Debug.Log(": \"Shield was still online!\"");
            Debug.Log(": Damage dealt to Shield: " + shot.Damage);

            // Prevent the Shield from going into negative numbers:
            if (Data.Shield < shot.Damage) Data.Shield = 0;
            else
            {
                Data.Shield -= shot.Damage;
            }
        }
        // Health + Armor damage code:
        else
        {
			audioSource.clip = painSound;
			audioSource.Play();            

            // Get the amount of damage that will go to our Armor, based on how effective our Armor is.
            float amountToArmor = shot.Damage * Data.ArmorEffectiveness;
            // Get the amount of damage going to our Health.
            float amountToHealth = shot.Damage - amountToArmor;

            // Apply the calculated amount's of damage to their respective values:
            Data.Armor -= amountToArmor;
            Data.Health -= amountToHealth;

            // Check if we applied too much damage to our Armor:
            if (Data.Armor < 0)
            {
                // Apply the overflow to our Health:
                Data.Health += Data.Armor;
                // Reset our Armor:
                Data.Armor = 0;
            }

			if(Data.Health < 1)
			{
				audioSource.clip = deathSound;
				audioSource.Play();

				GameManager.Instance.Load();
			}
        }
    }

	public void IndexShop(Shop in_shop)
	{
		if(in_shop != null)
		{
			IndexedShop = in_shop;
			
			GetComponent<FirstPersonController>().m_MouseLook.SetCursorLock(false);
			GetComponent<FirstPersonController>().enabled = false;
			
			GUIController.Instance.DisplayShopMenu(in_shop);
		}
		else
		{
			IndexedShop = null;

			GetComponent<FirstPersonController>().enabled = true;
			GetComponent<FirstPersonController>().m_MouseLook.SetCursorLock(true);

			GUIController.Instance.DisplayShopMenu(null);
		}
	}

	void Update()
	{
		GUIController.Instance.UpdateStats(Data.Health, Data.Armor, Data.Shield);

		if(IndexedShop != null)
		{
			if(Input.GetButtonDown("Cancel")) IndexShop(null);
		}
		else
		{
			if(Input.GetMouseButton(0))
			{
				weapon.Fire();
			}
			
			// Test for scroll wheel activity:
			float scroll = Input.GetAxis("Mouse ScrollWheel");
			if(scroll != 0)
			{
				if(scroll < 0)
				{
					// Prevent us from going past the end of our inventory:
					weaponIndex += weaponIndex < Data.Inventory.Count - 1 ? 1 : 0;
				}
				else
				{
					// Prevent us from going below -1:
					weaponIndex -= weaponIndex > -1 ? 1 : 0;
				}				

				if(weaponIndex >= 0)
				{
					SetActiveWeapon(Data.Inventory[weaponIndex]);
				}
				else
                {
                    SetActiveWeapon(Data.StartingWeapon);
                }
			}
		}

		if(Input.GetButton("Show Objectives"))
		{
			GUIController.Instance.DisplayObjectives(Data.Objectives, 1.0f);
		}
		else GUIController.Instance.HideObjectives();
	}
}
