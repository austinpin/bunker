using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestEntity : MonoBehaviour, Entity
{
	private float health = 100f;

	public void OnDamage(Shot shot)
	{
		Debug.Log(name + ": \"Hit for " + shot.Damage + " damage!\"");

		health -= shot.Damage;

		if(health < 0)
		{
			Debug.Log(name + " was killed!");

			Destroy(gameObject);
		}
	}
}
