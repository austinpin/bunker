using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* - BreakableDoor.cs
 * - By Nathan Penny
 * This entity destroys itself, and spawns a particle system gameObject if
 * hit with an explosive. It also has trigger functionality for activating music, etc.
 */
public class BreakableDoor : MonoBehaviour, Entity
{
	[SerializeField] private new ParticleSystem particleSystem;
	[SerializeField] private GameObject trigger;

	public void OnDamage(Shot shot)
	{
		if(shot.Type == ShotType.EXPLOSIVE)
		{
            ITriggerable trig = trigger.GetComponent<ITriggerable>();
            if (trig != null) trig.Trigger();

            Instantiate(particleSystem, transform.position, Quaternion.identity);
			Destroy(gameObject);
		}
	}
}
