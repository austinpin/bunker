﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* - ExplosiveBarrel.cs
 * - By Nathan Penny
 * This entity has a trigger on it that adds any enemies near enough to it to a list.
 * When the entity is destroyed, it created an explosive particle system, and deals
 * damage to all surrounding entities!
 */
public class ExplosiveBarrel : MonoBehaviour, Entity
{
	[SerializeField] private uint health;
	[SerializeField] private Shot shot;
    [SerializeField] private new ParticleSystem particleSystem;

	private List<Entity> toDamage;

	void Awake()
	{
		toDamage = new List<Entity>();
	}

	void OnTriggerEnter(Collider collider)
	{
		Entity entity = collider.GetComponent<Entity>();

		if(entity != null)
		{
			toDamage.Add(entity);
		}
	}

	void OnTriggerExit(Collider collider)
	{
		Entity entity = collider.GetComponent<Entity>();

		if(entity != null)
		{
			toDamage.Remove(entity);
		}
	}

    public void OnDamage(Shot damage)
	{
		if(damage.Damage > health) health = 0;
		else health -= damage.Damage;

		if(health < 1)
		{
			foreach(Entity entity in toDamage)
			{
				entity.OnDamage(shot);
			}

            Instantiate(particleSystem, transform.position, Quaternion.identity);
			Destroy(gameObject);
		}
	}
}
