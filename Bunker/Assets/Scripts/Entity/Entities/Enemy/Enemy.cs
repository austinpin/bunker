﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* - Enemy.cs
 * - By Nathan Penny
 * This entity take a player as argument, and will attempt,
 * to cast a ray towards it constantly. If the ray is unobstructed,
 * the enemy goes into attack mode, and begins firing a weapon at the player.
 */
public class Enemy : MonoBehaviour, Entity
{
	[SerializeField] public uint health;

	private Weapon weapon;
	[SerializeField] private string weaponPath;

    [SerializeField] private AudioClip[] alertSounds;
    [SerializeField] private AudioClip[] deathSounds;
    
	[SerializeField] private AudioSource audioSource;
    [SerializeField] private LayerMask windowLayer;
    [SerializeField] private Transform player;
	
	private bool idling = true;

	private float dTime = 1.0f;
	private float dFrac = 0.0f;

    // Use this for initialization
    void Start()
    {
		Weapon newWeapon = Resources.Load<GameObject>(weaponPath).GetComponent<Weapon>();

		if(newWeapon != null)
		{
			weapon = Instantiate(newWeapon, transform);

			weapon.camera = transform;
		}
		else
		{
			Debug.Log("No weapon script attached to " + weaponPath + "!");

			gameObject.SetActive(false);
		}
	}

    // Update is called once per frame
	void Update()
    {
		if(health < 1)
		{
			if(!audioSource.isPlaying)
			{
				Destroy(gameObject);
			}

			return;
		}

		dFrac += Time.deltaTime;

		// Decision Time!:
		if(dFrac > dTime)
		{
			dFrac = 0.0f;

            Ray ray = new Ray(transform.position, (player.position - transform.position));
			RaycastHit rayHit;
            Physics.Raycast(ray, out rayHit, 30, ~windowLayer);

            if (rayHit.collider != null)
            {
                if (idling)
			    {
					if(rayHit.collider.gameObject == player.gameObject)
					{
                        dTime = 0.5f;

						audioSource.clip = alertSounds[Random.Range(0, alertSounds.Length)];
						audioSource.Play();

						idling = false;
					}
				}
                else
                {
                    transform.LookAt(player);

                    weapon.Fire();
                }
            }
		}
	}

	public void OnDamage(Shot shot)
	{
		if(shot.Damage > health) health = 0;
		else health -= shot.Damage;

		if(health < 1)
		{
			audioSource.clip = deathSounds[Random.Range(0, deathSounds.Length)];
			audioSource.Play();

			GetComponent<Collider>().enabled = false;
			GetComponent<Renderer>().enabled = false;
		}
	}
}
