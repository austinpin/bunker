﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerTest : MonoBehaviour, ITriggerable
{
	public void Trigger()
	{
		StartCoroutine("MoveForwards");
	}

	IEnumerator MoveForwards()
	{
		while(true)
		{
			transform.Translate(Time.deltaTime, 0, 0);
			yield return null;
		}
	}
}
