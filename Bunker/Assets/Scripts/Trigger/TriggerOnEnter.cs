﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerOnEnter : MonoBehaviour
{
	[SerializeField] private new Collider collider;
	[SerializeField] private GameObject[] triggers;

	void OnTriggerEnter(Collider subject)
	{
		if(subject.gameObject.CompareTag("Player"))
		{
			foreach(GameObject obj in triggers)
			{
				ITriggerable trigger = obj.GetComponent<ITriggerable>();

				if(trigger != null) trigger.Trigger();
			}

			Destroy(gameObject);
		}
	}
}
