﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{
	[SerializeField] private GameObject[] triggers;

	void Start()
	{
		foreach(GameObject trigger in triggers)
		{
			ITriggerable trig = trigger.GetComponent<ITriggerable>();
			
			if(trig != null) trig.Trigger();
		}
	}
}
