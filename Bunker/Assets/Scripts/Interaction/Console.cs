﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class Console : MonoBehaviour, IInteractable
{
	[SerializeField] private Animator door;
	
	[SerializeField] private string animationState;

	[SerializeField] private PlayableDirector blastdoor;

	public void Interacted()
	{
		//door.SetBool("isOpen", true);
		blastdoor.Play();
	}
}
