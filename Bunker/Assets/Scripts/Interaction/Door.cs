﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* - Door.cs
 * - By Nathan Penny
 * This interactable object will only play it's opening animation if unlocked.
 * Otherwise, it will tell the player to go f*** themselves.
 */
public class Door : MonoBehaviour, IInteractable
{
	[SerializeField] private Animator animator;
	[SerializeField] private string lockedMessage;
    [SerializeField] private AudioSource audioSource;

	public  bool locked = false;
	private bool isOpen = false;

	public void Interacted()
	{
		if(locked)
		{
			// Inform the player the door is locked:
			GUIController.Instance.InfoText_Print(lockedMessage, 4.0f, true);
		}
		else
		{
			if(isOpen)
			{
                audioSource.Play();

                isOpen = false;
				animator.SetBool("isOpen", false);
			}
			else
			{
                audioSource.Play();

                isOpen = true;
				animator.SetBool("isOpen", true);
			}
		}
	}
}
