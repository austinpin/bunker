﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleButton : MonoBehaviour, IInteractable
{
	public void Interacted()
    {
        PlayerEntity ent = GetComponent<PlayerEntity>();

        Shot shot = new Shot();
        shot.Damage = 50;
        shot.Type = ShotType.BULLET;

        if (ent != null) ent.OnDamage(shot);
	}
}
