﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact : MonoBehaviour

//interact system originaly by Austin s1604097
//edited by

//script executes the interated function in scipts with the interaction interface

{
	// Update is called once per frame
	void Update ()
    {
		//create a ray coming out of the frame and a varaible to store data about what was hit
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;

		//check if the ray hits a collider.
		if (Physics.Raycast (ray, out hit))
        {
			//grab the sctipt with the interactable interface
            IInteractable subject = hit.collider.GetComponent<IInteractable>();

			//if the target has the interact interface
			if (subject != null)
            {
				//show that what the player is looking at can be interacted with (feedback)
				GUIController.Instance.InfoText_Print("Press \"e\" to use.", float.PositiveInfinity, false);

				//if the player decides to interact with it
				if (Input.GetButtonDown("Use"))
                {
					subject.Interacted();
				}
			}
			//hide the text
			else GUIController.Instance.InfoText_Print("", float.PositiveInfinity, true);

		}
	}
}
