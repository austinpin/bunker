﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Keycard : MonoBehaviour, IInteractable
{
	[SerializeField] private string collectText;
	[SerializeField] private Door[] doors;

	public void Interacted()
	{
		GUIController.Instance.InfoText_Print(collectText, 5.0f, true);

		foreach(Door door in doors)
		{
			door.locked = false;
		}

		Destroy(gameObject);
	}
}
