﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct ShopData
{
	[SerializeField]
	public List<WeaponData> Inventory;
}

/* - Shop.cs
 * - By Nathan Penny
 * This shop script is responsible for storing the weapon metadata needed by player to create weapons.
 */
public class Shop : MonoBehaviour
{
	[SerializeField]
	private string pathToData = "shop.json";
	
	[SerializeField]
	public ShopData Data;
	[SerializeField]
	private bool populateInventory = false;	

	// This function populates our inventory with the item's from a given file:
	void PopulateInventory()
	{
		ShopData masterData = 
		JsonUtility.FromJson<ShopData>(GameManager.Instance.LoadJson(pathToData));
	
		// This populates our current inventory with the item's from masterData, but in a random
		// order?
		Data = masterData;
	}
	
	void Start()
	{
		if(populateInventory) PopulateInventory();

	    foreach (WeaponData S in Data.Inventory)
	    {
	        Debug.Log(name + ": \"Has a " + S + ".\"");
	    }
	}
	
	public PlayerData indexedPlayer;
	public void Buy()
	{
		// Check to see if the indexedPlayer has enough credits:
	    if(indexedPlayer.Credits >= indexedWeapon.Value)
	    {
	        indexedPlayer.Credits -= indexedWeapon.Value;
	        indexedPlayer.Inventory.Add(new WeaponData(indexedWeapon));
	    }
		else OnCantAfford();
	}
	public void Sell()
	{
		// Check to see if the indexedWeapon belongs to us or not:
		if(indexedPlayer.Inventory.Contains(indexedWeapon))
		{
			Debug.LogError("Player contains weapon!");
		}

		if(!Data.Inventory.Contains(indexedWeapon))
		{
			indexedPlayer.Credits += indexedWeapon.Value;
			indexedPlayer.Inventory.Remove(indexedWeapon);
		}
		else Debug.LogError("Player is trying to sell us our gun!");
	}
	
	public WeaponData indexedWeapon;
	public void Index(int index)
	{
		if(index >= 0 && index < Data.Inventory.Count)
		{
			indexedWeapon = Data.Inventory[index];
		}
	}
	
	// This function defines what the behaviour is for when player's try to buy thing's they
	// can't afford:
	void OnCantAfford()
	{
		Debug.Log("You can't afford that!");
	    return;
	}
	// This function defines what the behaviour is for when player's try to buy somthing, but
	// we're out of stock. Alternatively, you could make it so that the player can't select
	// things that are out of stock:
	void OnOutOfStock()
	{
		Debug.Log("Sorry, kiddo. We're out of stock!");
	    return;
	}
	
	void OnTriggerEnter(Collider collider)
	{
		if(collider.CompareTag("Player"))
		{
			GUIController.Instance.InfoText_Print("Press 'e' to enter shop", float.PositiveInfinity, false);
		}
	}

	void OnTriggerStay(Collider collider)
	{
		if(collider.CompareTag("Player"))
		{
			if(Input.GetButtonDown("Use"))
			{
				PlayerEntity player = collider.GetComponent<PlayerEntity>();

				// Enter the shop:
				player.IndexShop(this);
				
				indexedPlayer = player.Data;
			}
		}
	}

	void OnTriggerExit(Collider collider)
	{
		if(collider.CompareTag("Player"))
		{
			GUIController.Instance.InfoText_Print("", 0f, true);
		}
	}
}
