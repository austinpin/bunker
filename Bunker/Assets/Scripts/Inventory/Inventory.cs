﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Inventory : MonoBehaviour, IInteractable
{
	public List<Item> inventory=new List<Item>();

	public void Interacted()
	{
		foreach (Item i in inventory)
        {
			Debug.Log (i.Data.Name);
		}
	}
}
