﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class ItemData
{
	[SerializeField]
	public string Name;
	[SerializeField]
	public string Description;
	[SerializeField]
	public Sprite Icon;
	[SerializeField]
	public uint Stock;
	[SerializeField]
	public uint Value;
};

// This is the base class for all items in our game. An item is any type that can be stored in an inventory:
public class Item : MonoBehaviour
{
	[SerializeField]
	public ItemData Data;

	public Item()
	{
		Data = new ItemData();
		Data.Name = "Default Name";
		Data.Description = "Uh Oh! If you're reading this, then you've found a BUG. Please tell someone. ;)";
		Data.Icon = Resources.Load<Sprite> ("Icons/"+Data.Name);
	}

	public Item(string name, string description)
	{
		Data = new ItemData();
		Data.Name = name;
		Data.Description = description;
	}
}
