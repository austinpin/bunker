﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIInventory : MonoBehaviour
{
	[SerializeField] private PlayerEntity player;
	[SerializeField] private Sprite FallBackSprite;
	[SerializeField] private List<Image> inventorySlots;

	[SerializeField]
	private Text itemNameText;
	[SerializeField]
	private Text itemDescText;
	[SerializeField]
	private Text itemCostText;

	[SerializeField]
	private Text playerCreditsText;
	[SerializeField]
	private List<Image> playerItems;

	[SerializeField]
	private Shop indexedShop;
	public void Populate(Shop in_shop)
	{
		if(in_shop != null && player != null)
		{
			indexedShop = in_shop;

			// Populate Inventory items:
			for(int i = 0; (i < in_shop.Data.Inventory.Count && i < inventorySlots.Count); ++i)
			{
				Sprite sprite = Resources.Load<Sprite>(in_shop.Data.Inventory[i].Icon);

				/* Only load a sprite from the inventory path if it's valid */
				if(sprite == null) 
				{
					sprite = FallBackSprite;
				}

				inventorySlots[i].sprite = sprite;
			}

			// Populate Player items:
			for(int i = 0; i < playerItems.Count; ++i)
			{
				Sprite sprite = FallBackSprite;
				
				/* Attempt to load sprite from player data */
				if(i < player.Data.Inventory.Count)
				{
					sprite = Resources.Load<Sprite>(player.Data.Inventory[i].Icon);

					if(sprite == null)
					{
						Debug.Log("player slot " + i + " was null!");
						sprite = FallBackSprite;
					}
				}

				playerItems[i].sprite = sprite;
			}

			itemNameText.text = "";
			itemDescText.text = "Hello, and welcome to the shop!\n Here is where you can buy and sell your gear!";
			itemCostText.text = "";

			playerCreditsText.text = "Credits: " + player.Data.Credits;
		}
	}

	public void ShowData(WeaponData data)
	{
		itemNameText.text = data.Name;
		itemDescText.text = data.Description;
		itemCostText.text = "Cost: " + data.Value;
	}

	public void IndexPlayer(int index)
	{
		if(player == null) return;

		if(index >= 0 && index < player.Data.Inventory.Count)
		{
			WeaponData indexedWeapon = player.Data.Inventory[index];

			indexedShop.indexedWeapon = indexedWeapon;

			ShowData(indexedWeapon);
		}
	}

	public void Index(int index)
	{
		if(indexedShop == null) return;

		if(index >= 0 && index < inventorySlots.Count)
		{
			indexedShop.Index(index);

			WeaponData indexedWeapon = indexedShop.indexedWeapon;

			indexedShop.indexedWeapon = indexedWeapon;

			ShowData(indexedWeapon);
		}
	}

	public void Buy()
	{
		if(indexedShop == null) return;
		
		else 
		{
			indexedShop.Buy();
			Populate(indexedShop);
		}
	}

	public void Sell()
	{
		if(indexedShop == null) return;
		
		else 
		{
			indexedShop.Sell();
			Populate(indexedShop);
		}
	}

//		if(in_shop != null)
//		{
//			for (int y = 0; y < sizeY; y++)
//			{
//				for (int x = 0; x < sizeX; x++)
//				{
//					Image indexedTile = new GameObject
//					(
//						"Tile (" + x + ", " + y + ")"
//
//					).AddComponent<Image>();
//
//					indexedTile.gameObject.AddComponent<Button>();
//					indexedTile.transform.SetParent(transform);
//					indexedTile.GetComponent<RectTransform>().anchoredPosition 
//					= new Vector2
//					(
//						20 + (84 * x), -20 + (-84 * y)
//					);
//					// 84 is the size of the tile + gap. +20 at start to spawn
//					// tile 20 from the borders
//
//					indexedTile.name = "Tile (" + x + ", " + y + ")";
//
//					indexedTile.transform.localScale = new Vector3(1, 1, 1);
//					indexedTile.transform.localPosition = new Vector3
//					(
//						0, 0, 0
//					);
//
//					tileImages.Add(indexedTile.GetComponent<Image>());
//				}
//			}
//		}
//		else
//		{
//			foreach(Image p in tileImages) Destroy(p.gameObject);
//			
//			tileImages = new List<Image>();
//		}
}
