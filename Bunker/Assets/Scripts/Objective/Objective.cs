﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Objective
{
    // Shorthand used for referencing the objective in code:
    [SerializeField] public string Name; // = "HasRedKeycard"

    // What will be displayed to the Player:
    [SerializeField] public string Title; // = "The Red Keycard Conundrum"

    // The deciption of the objective dictated to the Player:
    [SerializeField] public string Description; // = "Find the red keycard to get access to Engineering."

    // Determine whether the objective has been completed:
    [SerializeField] public bool Completed;
}
