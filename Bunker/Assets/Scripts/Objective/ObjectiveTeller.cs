﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveTeller : MonoBehaviour
{
    private Collider col;

    [SerializeField] private string objectiveTitle;

	[SerializeField]
	private Objective objective;

    void Start()
    {
        col = GetComponent<Collider>();

        if (col == null)
        {
            Debug.LogError(name + ": No collider found! Exiting...");
            Destroy(this);
        }

        objective = GameManager.Instance.GetObjective(objectiveTitle);
        
        if(objective == null)
        {
        	Debug.LogWarning(name + ": No objective with title \"" + objectiveTitle + "\" can be found in GameManager! Exiting...");
        	Destroy(this);
        }
    }

	// Update is called once per frame
    void OnTriggerEnter(Collider collider)
    {
    	Debug.Log(name + ": Player entered objectiveTeller!");
    
        if (collider.gameObject.CompareTag("Player"))
        {
        	collider.gameObject.GetComponent<PlayerEntity>().Data.Objectives.Add(objective);
                
			GUIController.Instance.InfoText_Print("Objective Added! Press tab to view!", 4f, true);
        }
    }
}
