﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* - ParticleTimer.cs
 * - By Nathan Penny
 * This class will wait until a given particle system is finished playing,
 * before destroying itself(and hopefully the particle system).
 */
public class ParticleTimer : MonoBehaviour
{
    [SerializeField] private new ParticleSystem particleSystem;

    private void Update()
    {
        if(!particleSystem.isPlaying)
        {
            Destroy(gameObject);
        }
    }
}
